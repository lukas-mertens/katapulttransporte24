"use strict";

// Load plugins
const autoprefixer = require("gulp-autoprefixer");
const browsersync = require("browser-sync").create();
const cleanCSS = require("gulp-clean-css");
const del = require("del");
const gulp = require("gulp");
const header = require("gulp-header");
const merge = require("merge-stream");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const uglify = require("gulp-uglify");

// Load package.json for banner
const pkg = require('./package.json');

// Set the banner content
const banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
    ' */\n',
    '\n'
].join('');

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./"
        },
        port: 3000
    });
    done();
}

// BrowserSync reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Clean vendor
function clean() {
    return del(["./vendor/"]);
}

// Bring third party dependencies from node_modules into vendor directory
function modules() {
    // Bootstrap
    var bootstrap = gulp.src('./node_modules/bootstrap/dist/**/*')
        .pipe(gulp.dest('./vendor/bootstrap'));
    // Font Awesome CSS
    var fontAwesomeCSS = gulp.src('./node_modules/@fortawesome/fontawesome-free/css/**/*')
        .pipe(gulp.dest('./vendor/fontawesome-free/css'));
    // Font Awesome Webfonts
    var fontAwesomeWebfonts = gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/**/*')
        .pipe(gulp.dest('./vendor/fontawesome-free/webfonts'));
    // jQuery Easing
    var jqueryEasing = gulp.src('./node_modules/jquery.easing/*.js')
        .pipe(gulp.dest('./vendor/jquery-easing'));
    // jQuery
    var jquery = gulp.src([
        './node_modules/jquery/dist/*',
        '!./node_modules/jquery/dist/core.js'
    ])
        .pipe(gulp.dest('./vendor/jquery'));
    // Magnific Popup
    var magnific = gulp.src([
        './node_modules/magnific-popup/dist/*'
    ])
        .pipe(gulp.dest('./vendor/magnific-popup'));

    // Scrollreveal
    var scrollreveal = gulp.src([
        './node_modules/scrollreveal/dist/*.js'
    ])
        .pipe(gulp.dest('./vendor/scrollreveal'));

    // slick-carousel
    var slickFonts = gulp.src([
        './node_modules/slick-carousel/slick/**/*',
    ])
        .pipe(gulp.dest('./vendor/slick-carousel'));

    var slick = gulp.src([
        './node_modules/slick-carousel/slick/fonts/**/*',
    ])
        .pipe(gulp.dest('./vendor/slick-carousel/fonts'));
    var animate = gulp.src([
        './node_modules/animate.css/*.css',
    ])
        .pipe(gulp.dest('./vendor/animate.css'));

    return merge(
        bootstrap,
        fontAwesomeCSS,
        fontAwesomeWebfonts,
        jquery,
        jqueryEasing,
        magnific,
        scrollreveal,
        slickFonts,
        slick,
        animate
    );
}

// CSS task
function css() {
    return gulp
        .src("./scss/**/*.scss")
        .pipe(plumber())
        .pipe(sass({
            outputStyle: "expanded",
            includePaths: "./node_modules",
        }))
        .on("error", sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(gulp.dest("./css"))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest("./css"))
        .pipe(browsersync.stream());
}

// JS task
function js() {
    return gulp
        .src([
            './js/*.js',
            '!./js/*.min.js'
        ])
        .pipe(uglify())
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./js'))
        .pipe(browsersync.stream());
}

// Watch files
function watchFiles() {
    gulp.watch("./scss/**/*", css);
    gulp.watch(["./js/**/*", "!./js/**/*.min.js"], js);
    gulp.watch("./**/*.html", browserSyncReload);
}

function dist() {
    const distCss = gulp.src('./css/**/*')
        .pipe(gulp.dest('./public/css'));
    const distImg = gulp.src('./img/**/*')
        .pipe(gulp.dest('./public/img'));
    const distJs = gulp.src('./js/**/*.min.js')
        .pipe(gulp.dest('./public/js'));
    const distVendor = gulp.src('./vendor/**/*')
        .pipe(gulp.dest('./public/vendor'));
    const distIndex = gulp.src('./index.html')
        .pipe(gulp.dest('./public'));
    return merge(distCss, distImg, distJs, distVendor, distIndex);
}

// Define complex tasks
const vendor = gulp.series(clean, modules);
const build = gulp.series(vendor, gulp.parallel(css, js), dist);
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// Export tasks
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.vendor = vendor;
exports.build = build;
exports.watch = watch;
exports.default = build;
