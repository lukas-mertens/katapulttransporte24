(function($) {
  "use strict"; // Start of use strict

  var skills = ["Reisen", "Spezialtransporte", "Versanddienstleistungen"];
  var currentSkill = 0;

                $.fn.extend({
        animateCss: function(animationName, callback) {
            var animationEnd = (function(el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                return animations[t];
                }
            }
            })(document.createElement('div'));

            this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
            });

            return this;
        },
        });

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Scroll reveal calls
  window.sr = ScrollReveal();

  sr.reveal('.sr-icon-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-icon-2', {
    delay: 400,
    scale: 0
  });
  sr.reveal('.sr-icon-3', {
    delay: 600,
    scale: 0
  });
  sr.reveal('.sr-icon-4', {
    delay: 800,
    scale: 0
  });
  sr.reveal('.sr-button', {
    delay: 200,
    distance: '15px',
    origin: 'bottom',
    scale: 0.8
  });
  sr.reveal('.sr-contact-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-contact-2', {
    delay: 400,
    scale: 0
  });

  // Magnific popup calls
  $('.popup-gallery').magnificPopup({
    delegate: 'a:not(.link)',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
  });

  function changeHeaderText() {
    setTimeout(changeHeaderText, 10000);
    $("#headerText").animateCss("flipOutX", function() {
      $("#headerText").html(skills[currentSkill]);
      $("#headerText").animateCss("bounceIn");
      $("#headerText").attr("data-text", skills[currentSkill]);
    });
    if(currentSkill<skills.length-1) {
      currentSkill++;
    }
    else {
      currentSkill = 0;
    }
  }
  setTimeout(changeHeaderText, 5000);

  $('.portfolio-slider').slick({
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "fade",
    dots: true,
    infinite: true,
    responsive: true,
    slidesToShow: 1,
    speed: 500,
  });

  $("#price-calc").submit(function(e) {
    e.preventDefault();
    var price = 0;
    var url = "https://www.distance24.org/route.json?jsonp=_jqjsp&stops=" + 
              encodeURIComponent($("#source").val()) + "|" + 
              encodeURIComponent($("#dest").val());
    $.jsonp({
      url: url,
      success: function (result, txtStatus, xOptions) {
        price = (Math.round(result.distance*0.8)/100).toFixed(2);
        console.log(result);
        if(result.distances.length > 0) {
          var output = price+"€";
          $("#price").addClass("text-primary");
          $("#price").removeClass("text-danger");
        } else {
          var output = "Leider konnten wir Ihre angegebene Route nicht finden...";
          $("#price").addClass("text-danger");
          $("#price").removeClass("text-primary");
        }
        if($("#output").css("display") == "none") {
          $("#output").show(500);
            $("#price").html(output);
        } else {
          $("#output").hide(500);
          setTimeout(function() {
            $("#output").show(500);
            $("#price").html(output);
          }, 500)
        }
      }
    });
  });
})(jQuery); // End of use strict
